package com.servicedesk.portal.api.reporsitory;

import com.servicedesk.portal.api.document.TicketDocument;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;


public interface TicketMongoRepository extends MongoRepository<TicketDocument, String> {
    @Query("{status : {$ne : 'Closed'}}")
    List<TicketDocument> findTicketByStatus();

}
