package com.servicedesk.portal.api.service;

import com.servicedesk.portal.api.document.TicketDocument;
import com.servicedesk.portal.api.reporsitory.TicketMongoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Service
public class TicketService {
    @Autowired
    private TicketMongoRepository ticketMongoRepository;

    public TicketDocument createTicket(TicketDocument ticketDocument) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
        ticketDocument.setCreatedOn(dateFormat.format(new Date()));
        return ticketMongoRepository.save(ticketDocument);
    }

    public List<TicketDocument> getAllTicket() {

        return ticketMongoRepository.findTicketByStatus();
    }

    public void deleteTicket(String ticketNumber) {

        ticketMongoRepository.deleteById(ticketNumber);
    }

    public TicketDocument editTicket(TicketDocument ticketDocument) {

        return ticketMongoRepository.save(ticketDocument);

    }


}
