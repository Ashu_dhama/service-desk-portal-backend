package com.servicedesk.portal.api.document;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;


@Document(collection = "ticket")
public class TicketDocument {

    @Id
    @NotNull
    private String ticketNumber;
    @NotNull
    private String title;
    @NotNull
    private String email;
    @NotNull
    private String status;
    @NotNull
    private String problemDescription;
    @NotNull
    private String createdOn;
    @NotNull
    private int priorityLevel;


    public String getTicketNumber() {

        return ticketNumber;
    }

    public void setTicketNumber(String ticketNumber) {

        this.ticketNumber = ticketNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {

        this.title = title;
    }

    public String getEmail() {

        return email;
    }

    public void setEmail(String email) {

        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {

        this.status = status;
    }

    public String getProblemDescription() {

        return problemDescription;
    }

    public void setProblemDescription(String problemDescription) {

        this.problemDescription = problemDescription;
    }

    public int getPriorityLevel() {

        return priorityLevel;
    }

    public void setPriorityLevel(int priorityLevel) {

        this.priorityLevel = priorityLevel;
    }

    public String getCreatedOn() {

        return createdOn;
    }

    public void setCreatedOn(String createdOn) {

        this.createdOn = createdOn;
    }

    @Override
    public String toString() {
        return "ticket [ticketNumber=" + ticketNumber + ", title=" + title + ", problemDescription=" + problemDescription + ", priorityLevel=" + priorityLevel + ", email=" + email + ", status=" + status + ", createdOn=" + createdOn + ",]";
    }

}
