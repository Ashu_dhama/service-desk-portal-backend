package com.servicedesk.portal.api.controller;

import com.servicedesk.portal.api.document.TicketDocument;
import com.servicedesk.portal.api.service.TicketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api")
public class TicketController {
    @Autowired
    private TicketService ticketService;

    @PostMapping("/tickets")
    public TicketDocument createTicket(@RequestBody TicketDocument ticket) {
        System.out.println("create........");
        return ticketService.createTicket(ticket);
    }

    @GetMapping("/tickets")
    public List<TicketDocument> getAllTickets() {
        System.out.println("Get all tickets...");
        return ticketService.getAllTicket();
    }


    @DeleteMapping("/tickets/{ticketNumber}")
    public void deleteTicket(@PathVariable("ticketNumber") String ticketNumber) {
        System.out.println("Delete ticket with ticketNumber = " + ticketNumber + "...");
        ticketService.deleteTicket(ticketNumber);

    }

    @PutMapping("/tickets/update")
    public TicketDocument updateTicket(@RequestBody TicketDocument ticketDocument) {
        System.out.println("Update ticket with ID ");
        return ticketService.editTicket(ticketDocument);
    }
}


