package com.servicedesk.portal.api.service;

import com.servicedesk.portal.api.document.TicketDocument;
import com.servicedesk.portal.api.reporsitory.TicketMongoRepository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import org.junit.Test;
import org.mockito.Mockito;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import java.util.ArrayList;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class TicketServiceTest {

    @Autowired
    private TicketService ticketService;

    @MockBean
    private TicketMongoRepository ticketMongoRepository;

    @Test
    public void testCreateTicket() {

        TicketDocument ticket = new TicketDocument();
        ticket.setTicketNumber("33");
        ticket.setStatus("Open");
        ticket.setProblemDescription("Test working problem");
        ticket.setTitle("Test issues");
        ticket.setPriorityLevel(5);
        ticket.setEmail("as@ttu.ee");
        ticket.setCreatedOn("28-10-2019 10:51:46 PM");

        Mockito.when(ticketMongoRepository.save(ticket)).thenReturn(ticket);

        assertThat(ticketService.createTicket(ticket)).isEqualTo(ticket);
    }

    @Test
    public void testGetAllTicket() {
        TicketDocument ticketDocument1 = new TicketDocument();
        ticketDocument1.setTicketNumber("33");
        ticketDocument1.setStatus("Pending");
        ticketDocument1.setProblemDescription("Test working problem");
        ticketDocument1.setTitle("Test issues");
        ticketDocument1.setPriorityLevel(5);
        ticketDocument1.setEmail("as@ttu.ee");
        ticketDocument1.setCreatedOn("28-10-2019 10:51:46 PM");

        TicketDocument ticketDocument2 = new TicketDocument();
        ticketDocument2.setTicketNumber("3322");
        ticketDocument2.setStatus("Open");
        ticketDocument2.setProblemDescription("Test working problem with micro services");
        ticketDocument2.setTitle("Test issues /qa");
        ticketDocument2.setPriorityLevel(5);
        ticketDocument2.setEmail("as@ttu.ee");
        ticketDocument2.setCreatedOn("28-10-2019 10:51:46 PM");
        List<TicketDocument> ticketsList = new ArrayList<>();
        ticketsList.add(ticketDocument1);
        ticketsList.add(ticketDocument2);

        Mockito.when(ticketMongoRepository.findTicketByStatus()).thenReturn(ticketsList);

        assertThat(ticketService.getAllTicket()).isEqualTo(ticketsList);
    }

    @Test
    public void testDeleteTicket() {
        TicketDocument ticketDocument = new TicketDocument();
        ticketDocument.setTicketNumber("33");
        ticketDocument.setStatus("closed");
        ticketDocument.setProblemDescription("Test working problem");
        ticketDocument.setTitle("Test issues");
        ticketDocument.setPriorityLevel(5);
        ticketDocument.setEmail("as@ttu.ee");
        ticketDocument.setCreatedOn("28-10-2019 10:51:46 PM");

        Mockito.when(ticketMongoRepository.findById(ticketDocument.getTicketNumber())).thenReturn(java.util.Optional.of(ticketDocument));
        Mockito.when(ticketMongoRepository.existsById(ticketDocument.getTicketNumber())).thenReturn(false);

        assertFalse(ticketMongoRepository.existsById(ticketDocument.getTicketNumber()));
    }

    @Test
    public void testEditTicket() {
        TicketDocument ticketDocument = new TicketDocument();
        ticketDocument.setTicketNumber("33");
        ticketDocument.setStatus("closed");
        ticketDocument.setProblemDescription("Test working problem");
        ticketDocument.setTitle("Test issues");
        ticketDocument.setPriorityLevel(1);
        ticketDocument.setEmail("as@ttu.ee");
        ticketDocument.setCreatedOn("28-10-2019 10:51:46 PM");
        Mockito.when(ticketMongoRepository.findById(ticketDocument.getTicketNumber())).thenReturn(java.util.Optional.ofNullable(ticketDocument));

        ticketDocument.setPriorityLevel(5);

        Mockito.when(ticketMongoRepository.save(ticketDocument)).thenReturn(ticketDocument);

        assertThat(ticketService.editTicket( ticketDocument)).isEqualTo(ticketDocument);
    }
}


