package com.servicedesk.portal.api.controller;

import com.servicedesk.portal.api.document.TicketDocument;
import com.servicedesk.portal.api.service.TicketService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import static org.junit.Assert.assertEquals;
import static org.assertj.core.api.Assertions.assertThat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringRunner.class)
@WebMvcTest
public class TicketControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TicketService ticketService;

    @Test
    public void testCreateTicket() throws Exception {
        TicketDocument mockTicketDocument = new TicketDocument();
        mockTicketDocument.setTicketNumber("33");
        mockTicketDocument.setStatus("pending");
        mockTicketDocument.setProblemDescription("Test working problem");
        mockTicketDocument.setTitle("Test issues");
        mockTicketDocument.setPriorityLevel(5);
        mockTicketDocument.setEmail("ashudhama32@gmail.com");
        mockTicketDocument.setCreatedOn("28-10-2019 10:51:46 PM");
        String inputInJson = this.mapToJson(mockTicketDocument);

        String Url = "/api/tickets";

        Mockito.when(ticketService.createTicket(Mockito.any(TicketDocument.class))).thenReturn(mockTicketDocument);
        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(Url)
                .accept(MediaType.APPLICATION_JSON).content(inputInJson)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        MockHttpServletResponse response = result.getResponse();

        String outputInJson = response.getContentAsString();

        assertThat(outputInJson).isEqualTo(inputInJson);
        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    @Test
    public void testGetAllTicket() throws Exception {
        TicketDocument mockTicketDocument = new TicketDocument();
        mockTicketDocument.setTicketNumber("33");
        mockTicketDocument.setStatus("Pending");
        mockTicketDocument.setProblemDescription("Test working problem");
        mockTicketDocument.setTitle("Test issues");
        mockTicketDocument.setPriorityLevel(5);
        mockTicketDocument.setEmail("as@ttu.ee");
        mockTicketDocument.setCreatedOn("28-10-2019 10:51:46 PM");

        TicketDocument mockTicketDocument1 = new TicketDocument();
        mockTicketDocument1.setTicketNumber("3");
        mockTicketDocument1.setEmail("krisit@ttu.ee");
        mockTicketDocument1.setProblemDescription("server not responding");
        mockTicketDocument1.setTitle("server issues");
        mockTicketDocument1.setPriorityLevel(5);
        mockTicketDocument1.setStatus("Open");
        mockTicketDocument1.setCreatedOn("28-10-2019 10:51:46 PM");
        List<TicketDocument> ticketDocumentList = new ArrayList<>();
        ticketDocumentList.add(mockTicketDocument);
        ticketDocumentList.add(mockTicketDocument1);

        Mockito.when(ticketService.getAllTicket()).thenReturn(ticketDocumentList);

        String Url = "/api/tickets";

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(Url)
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String expectedJson = this.mapToJson(ticketDocumentList);

        String outputInJson = result.getResponse().getContentAsString();
        assertThat(outputInJson).isEqualTo(expectedJson);

    }

    @Test
    public void testDeleteTicket() throws Exception {
        String Url = "/api/tickets/3";

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.delete(Url)).andReturn();

        int status = mvcResult.getResponse().getStatus();

        assertEquals(HttpStatus.OK.value(), status);
    }

    @Test
    public void testUpdateTicket() throws Exception {
        String Url = "/api/tickets/update";

        TicketDocument ticketDocument = new TicketDocument();
        ticketDocument.setTicketNumber("33");
        ticketDocument.setStatus("Pending");
        ticketDocument.setProblemDescription("Test working problem");
        ticketDocument.setTitle("Test issues");
        ticketDocument.setPriorityLevel(1);
        ticketDocument.setEmail("as@ttu.ee");
        ticketDocument.setCreatedOn("28-10-2019 10:51:46 PM");

        Mockito.when(ticketService.editTicket(Mockito.any(TicketDocument.class))).thenReturn(ticketDocument);

        ticketDocument.setStatus("Open");

        String inputJson = this.mapToJson(ticketDocument);

        RequestBuilder requestBuilder = MockMvcRequestBuilders
                .put(Url)
                .accept(MediaType.APPLICATION_JSON).content(inputJson)
                .contentType(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        MockHttpServletResponse response = result.getResponse();

        String outputInJson = response.getContentAsString();

        assertThat(outputInJson).isEqualTo(inputJson);
        assertEquals(HttpStatus.OK.value(), response.getStatus());
    }

    private String mapToJson(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }

}
